class cal():
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def add(self):
        return self.a + self.b
    def sub(self):
        return self.a - self.b
    def multiply(self):
        return self.a * self.b
    def divide(self):
        return self.a / self.b

a = int(input('Enter First number : '))
b = int(input('Enter Second number : '))
obj=cal(a,b)
while True:
    def menu():
        x = ('1. Add \n2. Sub \n3. Multiply \n4. Divide \n0.Exit')
        print(x)
    menu()
    ch = int(input('Please select one of the following : '))
    if ch == 1:
        print("Result: ",obj.add())
    elif ch == 2:
        print("Result: ",obj.sub())
    elif ch == 3:
        print("Result: ",obj.multiply())
    elif ch == 4:
        if obj.b!=0:
            print("Result: ",obj.divide())
        else:
            print("Division by 0 is not possible")
    elif ch == 0:
        break
    else:
        print('Invalid option. Choose From The menu')

print()
